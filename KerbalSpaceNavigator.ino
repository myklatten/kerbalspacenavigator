#include <usbhid.h>
#include <hiduniversal.h>
#include <usbhub.h>
#include <string.h>
#include "keycodes_uk.h"

// ==== Settings region ===

#define T_CYCLE (0.333) // keypress cycle time in seconds

#define SENSITIVITY (1.5) // movement sensitivity: 
  // 1.0 => need to move mouse to max position to get key pressed continuously
  // 2.0 => need to move mouse to half of max position to get key pressed continuously

const uint8_t key_map[][6][2] = {
  // For each of the 6 axes of movement (three translation and three rotation) there are two key-codes
  //  defined: one for negative movement on that axis and one for positive.
  {
      // mapping 0: rotation only
      // Translation axes
      {0, 0},
      {0, 0},
      {0, 0},
      // Rotation axes
      {KEY_W, KEY_S}, // pitch
      {KEY_A, KEY_D}, // yaw
      {KEY_E, KEY_Q} // roll
  },
  {
      // mapping 1: EVA
      // Translation axes
      {KEY_A, KEY_D}, // left/right
      {KEY_W, KEY_S}, // forward/backward
      {KEY_Left_Shift, KEY_Left_Control}, // up/down
      // Rotation axes
      {0, 0},
      {0, 0},
      {KEY_Q, KEY_E} // rotate left/right
  },
  {
      // mapping 2: RCS
      // Translation axes
      {KEY_J, KEY_L}, // left/right
      {KEY_H, KEY_N}, // forward/backward
      {KEY_I, KEY_K}, // up/down 
      // Rotation axes
      {KEY_W, KEY_S}, // pitch
      {KEY_A, KEY_D}, // yaw
      {KEY_E, KEY_Q} // roll
  }
};

// ==== End of settings region ===

// Satisfy IDE, which only needs to see the include statment in the ino.
#ifdef dobogusinclude
#include <spi4teensy3.h>
#include <SPI.h>
#endif

#include "hidspacenavigatorrptparser.h"
#include "KeyboardLL.h"
#include "keycodes_uk.h"
#include "indicate_keymap.h"

USB Usb;
USBHub Hub(&Usb);
HIDUniversal Hid(&Usb);
SpaceNavigatorState mySpaceNavigatorState;
SpaceNavigatorReportParser mySpaceNavigator(&mySpaceNavigatorState);

#define N_KEY_MAPPINGS (sizeof(key_map)/sizeof(key_map[0]))

uint8_t key_for_axis[6][2];

uint32_t t_start;
int8_t rampdir;
uint32_t t_halfcycle;
int32_t scalefac;
int32_t mouse_scaled[6];
int current_mapping;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH); 
  indicate_keymap_init();

  if (Usb.Init() == -1) {
    //   Serial.println("OSC did not start.");
    while (1) {
      digitalWrite(LED_BUILTIN, LOW); 
      delay(990);
      digitalWrite(LED_BUILTIN, HIGH); 
      delay(10);
    }
  }

  delay(200);
  
  if (!Hid.SetReportParser(0, &mySpaceNavigator)) {
    // ErrorMessage<uint8_t> (PSTR("SetReportParser"), 1);
    while (1) {
      digitalWrite(LED_BUILTIN, LOW); 
      delay(880);
      digitalWrite(LED_BUILTIN, HIGH); 
      delay(10);
      digitalWrite(LED_BUILTIN, LOW); 
      delay(100);
      digitalWrite(LED_BUILTIN, HIGH); 
      delay(10);
    }
  }

  t_start = micros();
  rampdir = 1;

  t_halfcycle = (uint32_t)(T_CYCLE * 0.5e6); // half-cycle time in microseconds
  scalefac = (int32_t)(T_CYCLE * 0.5e6 * SENSITIVITY / 350.0); // scaling from axis position to microseconds

  current_mapping = 0;
  memcpy(key_for_axis, key_map[current_mapping], sizeof(key_for_axis));
  indicate_keymap(current_mapping);
  spacenav_led_set(0);
}

void spacenav_led_set(bool want_on) {
  static uint8_t sendbuf[2];
  // You can turn the LEDs on by sending the packet: 0x04 0x01. Turn them off with 0x04 0x00.
  sendbuf[0] = 0x04;
  sendbuf[1] = want_on ? 1 : 0;
  Hid.SndRpt(2, sendbuf);
}

void loop() {
  // Update mySpaceNavigatorState
  Usb.Task();

  // SpaceNavigator buttons select key mapping to use
  if (mySpaceNavigatorState.button_clicks[0] || mySpaceNavigatorState.button_clicks[1]) {
    current_mapping += mySpaceNavigatorState.button_clicks[0] - mySpaceNavigatorState.button_clicks[1];
    mySpaceNavigatorState.button_clicks[0] = 0;
    mySpaceNavigatorState.button_clicks[1] = 0;
    current_mapping = (current_mapping + 4*N_KEY_MAPPINGS) % N_KEY_MAPPINGS;
    memcpy(key_for_axis, key_map[current_mapping], sizeof(key_for_axis));
    indicate_keymap(current_mapping);
  }

  // Generate the triangle waveform as a function of elapsed time (micros())
  uint32_t a;
  bool newcyc = 0;
  while (1) {
    a = micros() - t_start;
    if (a <= t_halfcycle)
      break;
    t_start += t_halfcycle;
    rampdir = -rampdir;
    newcyc = 1;
  }
  if (rampdir < 0)
    a = t_halfcycle - a;

  if (newcyc) {
    // Update scaled axis positions at the start of each half-cycle
    digitalWrite(LED_BUILTIN, rampdir >= 0 ? HIGH : LOW); 
    for (int idx=0; idx<6; idx++)
      mouse_scaled[idx] = scalefac * (int32_t)mySpaceNavigatorState.axpos[idx];
  }

  // Compare each axis input value with the current value of the triangle waveform,
  //  and press key if a < |mouse_scaled[idx]|
  for (int idx=0; idx<6; idx++) {
    uint8_t kwant = 0; // default is 0, which means key not pressed
    if (mouse_scaled[idx] < 0) {
      if (a < (-mouse_scaled[idx]))
        kwant = key_for_axis[idx][0];
    }
    else {
      if (a < mouse_scaled[idx])
        kwant = key_for_axis[idx][1];
    }
    KeyboardLL.setKeySlot(idx, kwant);
  }
  KeyboardLL.sendReport();
}
