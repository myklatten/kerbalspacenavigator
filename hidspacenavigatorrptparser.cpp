#include "hidspacenavigatorrptparser.h"

SpaceNavigatorReportParser::SpaceNavigatorReportParser(SpaceNavigatorState *evt) :
spaceNavigatorState(evt)
{
}

void SpaceNavigatorReportParser::Parse(USBHID *hid, bool is_rpt_id, uint8_t len, uint8_t *buf) {
    const SpaceNavigatorEventData *evt = (const SpaceNavigatorEventData*)buf;

    // Translation vector
    if (evt->rptid == 1) {
      spaceNavigatorState->axpos[0] = evt->axpos[0];
      spaceNavigatorState->axpos[1] = evt->axpos[1];
      spaceNavigatorState->axpos[2] = evt->axpos[2];
      if (len >= 1 + 6 + 6) {
        // Rotation vector for different type of SpaceNavigator - untested
        spaceNavigatorState->axpos[3] = evt->axpos[3];
        spaceNavigatorState->axpos[4] = evt->axpos[4];
        spaceNavigatorState->axpos[5] = evt->axpos[5];
      }
    }

    // Rotation vector
    else if (evt->rptid == 2) {
      spaceNavigatorState->axpos[3] = evt->axpos[0];
      spaceNavigatorState->axpos[4] = evt->axpos[1];
      spaceNavigatorState->axpos[5] = evt->axpos[2];
    }

    // Buttons
    else if (evt->rptid == 3) {
      // detect buttons going from unpressed->pressed
      uint32_t btn_down = (uint32_t)evt->axpos[0] & ~spaceNavigatorState->buttons;
      if (btn_down & 1)
        spaceNavigatorState->button_clicks[0] ++;
      if (btn_down & 2)
        spaceNavigatorState->button_clicks[1] ++;

      spaceNavigatorState->buttons = (uint32_t)evt->axpos[0];
    }

}
