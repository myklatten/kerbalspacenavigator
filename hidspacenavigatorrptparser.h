#if !defined(__HIDSPACENAVIGATORRPTPARSER_H__)
#define __HIDSPACENAVIGATORRPTPARSER_H__

#include <usbhid.h>

// We will cast the packet from the mouse to this structure: relies on the micro being little-endian!
struct SpaceNavigatorEventData {
    uint8_t rptid;
    int16_t axpos[6];
};

class SpaceNavigatorState {
public:
    int16_t axpos[6];
    uint16_t buttons;
    uint16_t button_clicks[2];

    SpaceNavigatorState() { 
        for (int idx=0; idx<6; idx++)
            axpos[idx] = 0;
        buttons = 0;
        for (int idx=0; idx<2; idx++)
            button_clicks[idx] = 0;
    }

};

class SpaceNavigatorReportParser : public HIDReportParser {
    SpaceNavigatorState *spaceNavigatorState;
    
public:
    SpaceNavigatorReportParser(SpaceNavigatorState *evt);
    
    virtual void Parse(USBHID *hid, bool is_rpt_id, uint8_t len, uint8_t *buf);
};

#endif // __HIDSPACENAVIGATORRPTPARSER_H__
