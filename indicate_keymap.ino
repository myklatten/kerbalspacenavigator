
static const int led_pin[3] = {4, 3, 2};
static const int led_off = HIGH;
static const int led_on = LOW;

void indicate_keymap_init() {
    for (int i=0; i < sizeof(led_pin)/sizeof(led_pin[0]); i++) {
        pinMode(led_pin[i], OUTPUT);
        digitalWrite(led_pin[i], led_off); 
    }
}

void indicate_keymap(int keymap_idx) {
    for (int i=0; i < sizeof(led_pin)/sizeof(led_pin[0]); i++) {
        digitalWrite(led_pin[i], i==keymap_idx ? led_on : led_off); 
    }
}
